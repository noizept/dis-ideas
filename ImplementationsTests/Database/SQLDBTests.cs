﻿using System;
using System.Collections;
using System.Collections.Generic;
using Implementations.Comment;
using Implementations.Database;
using Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Implementations.IdeasImplementation;
using Implementations.Status;
using Implementations.UsersImplementation;
using Interfaces.Users;

namespace ImplementationsTests.Database
{
    [TestClass()]
    public class SQLDBTests
    {
        //====================================== TESTES USER ========================//
        [TestMethod()]
        public void RegistUserTest()
        {
            var db= SQLDB.GetDb();
            var aDic = new Hashtable
            {
                {"email", "teste@teste.com"},
                {"first_name", "teste_user"},
                {"last_name", "last_name"},
                {"password", "password"}
            };
            Assert.IsTrue(db.RegistUser(aDic));
        }



        [TestMethod()]
        public void DeleteUserTest()
        {
            var db = SQLDB.GetDb();
            Assert.IsTrue(db.DeleteUser("teste@teste.com"));
        }

        [TestMethod()]
        public void GetIdeaOwnerEmailTest()
        {
            Assert.AreEqual("dummy@dummy.com",SQLDB.GetDb().GetIdeaOwnerEmail(8));
        }

        [TestMethod()]
        public void GetUserByEmailTest()
        {
            Assert.AreEqual("dummy",SQLDB.GetDb().GetUserByEmail("dummy@dummy.com").FirstName);
        }

        [TestMethod()]
        public void GetAllUsersDataTableTest()
        {
            Assert.IsNotNull(SQLDB.GetDb().GetAllUsersDataTable());
        }

        //=================================== TESTES IDEA ========================//
        [TestMethod()]
        public void SaveIdeaTest()
        {
            var db = SQLDB.GetDb();
            User aUser=new Client();
            aUser.Email = "dummy@dummy.com";
            IIdeas aIdea = IdeaCreator.CreateIdea();
            aIdea.Contennt = "testeIdea";
            aIdea.Owner = aUser;
            aIdea.State = new Draft();
            aIdea.Title = "TestTitle";
            Assert.IsTrue(db.SaveIdea(aIdea));
        }

        [TestMethod()]
        public void UpdateIdeaTest()
        {
            var db = SQLDB.GetDb();
            var idea = IdeaCreator.CreateIdea();
            idea.Id = 8;
            idea.Contennt = "TESTEEEEUPDATE";
            idea.Title = "DDDDDD";
            idea.State = new Draft();
            Assert.IsTrue(db.UpdateIdea(idea));
        }

        [TestMethod()]
        public void GetLastIdeaIdTest()
        {
            var db = SQLDB.GetDb();
            Assert.AreNotEqual(0, db.GetLastIdeaId());

        }

        [TestMethod()]
        public void GetIdeasDataTableTest()
        {
            var db = SQLDB.GetDb();
            Interfaces.Users.User aUser = new Client();
            aUser.Email = "dummy@dummy.com";
            Assert.IsNotNull(db.GetIdeasDataTable(aUser));
        }

        [TestMethod()]
        public void GetIdeaTest()
        {
            var db = SQLDB.GetDb();
            Assert.AreEqual("DDDDDD",db.GetIdea(8).Title);
        }


        [TestMethod()]
        public void GetIdeasByTagsTest()
        {
            Assert.IsNotNull(SQLDB.GetDb().GetIdeasByTags("CU ADASD"));
        }

        //======================================== TAGS TEST =================///
        [TestMethod()]
        public void AddTagsTest()
        {
            IIdeas aIdea = IdeaCreator.CreateIdea();
            IList<string> tags = new List<string>();
            tags.Add("BAH");
            tags.Add("BEH");
            tags.Add("BUH");
            Assert.IsTrue(SQLDB.GetDb().AddTags(aIdea, tags));
        }

        [TestMethod()]
        public void DeleteTagsTest()
        {
            IIdeas adeia = IdeaCreator.CreateIdea();
            adeia.Id = 29;
            Assert.IsTrue(SQLDB.GetDb().DeleteTags(adeia));
        }

        [TestMethod()]
        public void GetTagListTest()
        {
            IIdeas aideia=new Idea();
            aideia.Id = 25;
            Assert.AreEqual("CU",SQLDB.GetDb().GetTags(aideia)[0].TagName);
        }



        //================================== VOTE TEST =======================//
        [TestMethod()]
        public void VoteTest()
        {
            var db = SQLDB.GetDb();
            Assert.IsTrue(db.Vote("dummy@dummy.com",8,3));
        }



        //======================== SUBSCRIPTION TEST =================== //

        [TestMethod()]
        public void SubUserTest()
        {
            Assert.IsFalse(SQLDB.GetDb().IsSubscribedToUser("1","1"));
            Assert.IsTrue(SQLDB.GetDb().SubscribeToUser("1", "1"));
            Assert.IsTrue(SQLDB.GetDb().UnSubscribeToUser("1", "1"));

        }

        [TestMethod()]
        public void SubToIdeaTest()
        {
            Assert.IsFalse(SQLDB.GetDb().IsSubscribedToIdea("1", 25));
            Assert.IsTrue(SQLDB.GetDb().SubscribeToIdea("1", 25));
            Assert.IsTrue(SQLDB.GetDb().UnSubscribeToIdea("1", 25));

        }

        [TestMethod()]
        public void GetAllUserSubscriptionTest()
        {
            Assert.IsTrue(SQLDB.GetDb().GetAllUserSubscription("1").Contains("1"));
        }


        //======================= CommentTest ===========================//
        [TestMethod()]
        public void AddCommentTest()
        {
            IComments aComment = CommentCreator.CreateComment();
            aComment.Content = "THIS IS A TEST COMMENT";
            aComment.Creator = "dummy@dummy.com";
            aComment.Idea = 25;
            Assert.IsTrue(SQLDB.GetDb().AddComment(aComment));
        }
    }
}