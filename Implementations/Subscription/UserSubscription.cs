﻿using Implementations.Database;
using Interfaces.Subsription;

namespace Implementations.Subscription
{
    public class UserSubscription : ISubscription
    {
        private string _selfEmail;

        public UserSubscription(string aSelfEmail)
        {
            _selfEmail = aSelfEmail;
        }

        public bool Execute(string aSubscriptionEmail)
        {
            return SQLDB.GetDb().SubscribeToUser(_selfEmail, aSubscriptionEmail);
        }
    }
}