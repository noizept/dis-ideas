﻿using System;
using Implementations.Database;
using Interfaces.Subsription;

namespace Implementations.Subscription
{
    public class IdeaSubscription : ISubscription
    {

        string _selfEmail;

        public IdeaSubscription(string selfEmail)
        {
            _selfEmail = selfEmail;
        }

        public bool Execute(string aIdeaId)
        {
            return SQLDB.GetDb().SubscribeToIdea(_selfEmail, Convert.ToInt32(aIdeaId));
        }
    }
}