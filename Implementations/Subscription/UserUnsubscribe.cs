﻿using Implementations.Database;
using Interfaces.Subsription;

namespace Implementations.Subscription
{
    public class UserUnsubscribe : ISubscription
    {
        private string _selfEmail ;

        public UserUnsubscribe(string selfEmail)
        {
            _selfEmail = selfEmail;
        }


        public bool Execute(string aSubscriptionEmail)
        {
            return SQLDB.GetDb().UnSubscribeToUser(_selfEmail, aSubscriptionEmail);
        }
    }
}