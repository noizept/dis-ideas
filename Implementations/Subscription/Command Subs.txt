﻿
//COMMAND
ISubscription aSub=new UserSubscription(MYEMAIL);


//INVOKER
SubscriptionInvoker aInvoker= new SubscriptionInvoker();

aInvoker.ChangeCommand(aSub);
aInvoker.Execute(EMAIL_OF_WHO_I_WANT_TO_SUBSCRIBE);

ISubscription aSubIdea=new IdeaSubscription(MYEMAIL);
aInvoker.ChangeCommand(aSubIdea);

aInvoker.Execute(ID_OF_IDEA_I_WANT_TO_SUBSCRIBE);

