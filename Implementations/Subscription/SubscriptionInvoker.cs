﻿using Interfaces.Subsription;

namespace Implementations.Subscription
{
    public class SubscriptionInvoker
    {
        ISubscription _subscription;

        public void ChangeCommand(ISubscription aCommand)
        {
            _subscription = aCommand;
        }

        public bool Execute(string Parameter)
        {
            return _subscription.Execute(Parameter);
        }
    }
}