﻿using System;
using Implementations.Database;
using Interfaces.Subsription;

namespace Implementations.Subscription
{
    public class IdeaUnsubscription : ISubscription
    {
        string _selfEmail;

        public IdeaUnsubscription(string Email)
        {
            _selfEmail = Email;
        }


        public bool Execute(string aSubIdeaId)
        {
            return SQLDB.GetDb().UnSubscribeToIdea(_selfEmail, Convert.ToInt32(aSubIdeaId));
        }
    }
}