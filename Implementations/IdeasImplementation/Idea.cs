﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
using Interfaces.Users;

namespace Implementations.IdeasImplementation
{
    public class Idea : IIdeas
    {

        public Idea()
        {
            Tags=new List<ITag>();
        }

        public int Id { get; set; }

        public IList<ITag> Tags { get; set; }

        public DateTime CreaDateTime {  get; set; }
        public string Title { get; set; }
        public string Contennt { get; set; }
        public IStatus State { get; set; }

        public User Owner { get; set; }

    }
}
