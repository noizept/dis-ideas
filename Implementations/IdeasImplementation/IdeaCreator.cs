﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;

namespace Implementations.IdeasImplementation
{
    public class IdeaCreator
    {
        public static Idea CreateIdea()
        {
            return new Idea();
        }
    }
}
