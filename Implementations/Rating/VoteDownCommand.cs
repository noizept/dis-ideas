﻿using Implementations.Database;
using Interfaces;

namespace Implementations.Rating
{
    public class VoteDownCommand : IRating
    {

        string _email;

        public VoteDownCommand(string email)
        {
            _email = email;
        }

        public bool Execute(int IdeiaId)
        {
            return SQLDB.GetDb().Vote(_email, IdeiaId, 0);
        }
    }
}