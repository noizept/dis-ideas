﻿using Interfaces;

namespace Implementations.Rating
{
    public class VoteInvoker
    {
        IRating _commandUp;
        IRating _commandDown;

        public VoteInvoker(IRating aCommandUp,IRating aComandDown)
        {
            _commandUp = aCommandUp;
            _commandDown = aComandDown;
        }

        public bool UpVote(int aIdeaId)
        {
           return  _commandUp.Execute(aIdeaId);
        }

        public bool DownVote(int aIdeaId)
        {
            return _commandDown.Execute(aIdeaId);
        }
    }
}