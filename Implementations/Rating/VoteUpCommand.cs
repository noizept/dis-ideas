﻿using Implementations.Database;
using Interfaces;

namespace Implementations.Rating
{
    public class VoteUpCommand : IRating
    {
        string _email;

        public VoteUpCommand(string email)
        {
            _email = email;
        }

        public bool Execute(int IdeiaId)
        {
            return SQLDB.GetDb().Vote(_email, IdeiaId, 1);
        }
    }
}