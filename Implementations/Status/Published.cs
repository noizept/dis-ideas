﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;

namespace Implementations.Status
{
    public class Published : IStatus
    {

        public string GetStatus()
        {
            return "Published";
        }
    }
}
