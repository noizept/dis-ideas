﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Interfaces;
using Interfaces.DB;
using Interfaces.Users;
using MySql.Data.MySqlClient;


namespace Implementations.Database
{
    public class SQLDB : IDatabase, IDbIdeas, IDbSubscription, ILogin, IDbFeed, IStatistics
    {
        private static SQLDB _myDatabase;
        private static MySqlConnection _mySqlConnection;

        private SQLDB()
        {
        }

        public static SQLDB GetDb()
        {
            if (_myDatabase == null)
            {
                _myDatabase = new SQLDB();
                var server = "sviula.com";
                var database = "Ideias";
                var uid = "ideias";
                var password = "ideias";

                string connectionString = "SERVER=" + server + ";"
                                          + "DATABASE=" + database
                                          + ";" + "UID=" + uid + ";"
                                          + "PASSWORD=" + password + ";";
                _mySqlConnection = new MySqlConnection(connectionString);
            }
            return _myDatabase;
        }


        public bool RegistUser(Hashtable aDictionary)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText =
                        "INSERT INTO user(email,first_name,last_name,password,acc_type,visits,creation_date) " +
                        "VALUES(@param1,@param2,@param3,@param4,@param5,@param6,@param7)";
                    command.Parameters.AddWithValue("@param1", aDictionary["email"]);
                    command.Parameters.AddWithValue("@param2", aDictionary["first_name"]);
                    command.Parameters.AddWithValue("@param3", aDictionary["last_name"]);
                    command.Parameters.AddWithValue("@param4", aDictionary["password"]);
                    command.Parameters.AddWithValue("@param5", 0);
                    command.Parameters.AddWithValue("@param6", 0);
                    command.Parameters.AddWithValue("@param7", DateTime.Now);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public bool DeleteUser(string aEmail)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText = "DELETE FROM user Where email=@param1";
                    command.Parameters.AddWithValue("@param1", aEmail);
                    command.ExecuteNonQuery();
                    return true;

                }
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }

        }

        private void SaveIdeaParameters(ref MySqlCommand command, IIdeas aIdea)
        {
            command.CommandText = "INSERT INTO idea(owner,title,content,creation_date,state) " +
                                  "VALUES(@param1,@param2,@param3,@param4,@param5)";
            command.Parameters.AddWithValue("@param1", aIdea.Owner.Email);
            command.Parameters.AddWithValue("@param2", aIdea.Title);
            command.Parameters.AddWithValue("@param3", aIdea.Contennt);
            command.Parameters.AddWithValue("@param4", DateTime.Now);
            command.Parameters.AddWithValue("@param5", aIdea.State.GetStatus());

        }

        private void UpdateIdeaParameters(ref MySqlCommand command, IIdeas aIdea)
        {
            command.CommandText = "UPDATE idea SET " +
                                  "title=@param1, " +
                                  "content=@param2, " +
                                  "state=@param3" +
                                  " WHERE idIdeas=@param4";
            command.Parameters.AddWithValue("@param1", aIdea.Title);
            command.Parameters.AddWithValue("@param2", aIdea.Contennt);
            command.Parameters.AddWithValue("@param3", aIdea.State.GetStatus());
            command.Parameters.AddWithValue("@param4", aIdea.Id);
        }

        public DataTable GetAllUsersDataTable()
        {
            try
            {
                _mySqlConnection.Open();
                var command = _mySqlConnection.CreateCommand();
                command.CommandText = "SELECT * FROM user";
                var converter = new DataConverter();
                return converter.GetUsersTable(command.ExecuteReader());
            }

            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public bool UpdateLastVisit(string email)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText = "UPDATE user SET last_visit=@param1 WHERE email=@param2";
                    command.Parameters.AddWithValue("@param1", DateTime.Now);
                    command.Parameters.AddWithValue("@param2", email);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public bool IncrementVisits(string email)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText = "UPDATE user SET visits=visits+1 WHERE email=@param2";
                    command.Parameters.AddWithValue("@param2", email);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public bool SaveIdea(IIdeas aIdea)
        {
            try
            {
                var command = _mySqlConnection.CreateCommand();
                SaveIdeaParameters(ref command, aIdea);
                _mySqlConnection.Open();
                command.ExecuteNonQuery();
                return true;
            }

            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public bool UpdateIdea(IIdeas aIdea)
        {
            try
            {
                var command = _mySqlConnection.CreateCommand();
                UpdateIdeaParameters(ref command, aIdea);
                _mySqlConnection.Open();
                command.ExecuteNonQuery();
                return true;
            }

            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public bool DeleteIdea(IIdeas aIdea)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText = "DELETE FROM idea Where idIdeas=@param1";
                    command.Parameters.AddWithValue("@param1", aIdea.Id);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public User GetUserByEmail(string aEmail)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM user WHERE email=@param1";
                    command.Parameters.AddWithValue("@param1", aEmail);
                    var dataReader = command.ExecuteReader();
                    IDataConverter aDataConverter = new DataConverter();
                    return aDataConverter.GetUserObject(dataReader);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public string GetIdeaOwnerEmail(int aId)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM idea Where idIdeas=@param1";
                    command.Parameters.AddWithValue("@param1", aId);
                    MySqlDataReader reader = command.ExecuteReader();
                    return new DataConverter().GetUserEmailByIdea(reader);

                }
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }

        }

        public DataTable GetIdeasDataTable(Interfaces.Users.User aUser)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM idea WHERE owner=@param1";
                    command.Parameters.AddWithValue("@param1", aUser.Email);
                    var dataReader = command.ExecuteReader();
                    IDataConverter aDataConverter = new DataConverter();
                    return aDataConverter.GetIdeasTable(dataReader);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public int GetLastIdeaId()
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    var lastId = 0;
                    command.CommandText = "SELECT MAX(idIdeas) FROM idea";
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        lastId = (int) reader["MAX(idIdeas)"];
                    }
                    return lastId;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public IIdeas GetIdea(int aId)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM idea WHERE idIdeas=@param1";
                    command.Parameters.AddWithValue("@param1", aId);
                    var dataReader = command.ExecuteReader();
                    IDataConverter aDataConverter = new DataConverter();
                    return aDataConverter.GetIdea(dataReader);
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public DataTable GetIdeasByTags(string ListTags)
        {
            try
            {
                List<string> tags = ListTags.Split(Convert.ToChar(" ")).ToList();
                if (tags.Count == 0)
                    return null;

                _mySqlConnection.Open();
                var command = _mySqlConnection.CreateCommand();
                command.CommandText =
                    "SELECT idea.idIdeas,idea.title,idea.owner,idea.creation_date FROM idea,tag WHERE " +
                    "(tag.tag_name ='" + tags[0] + "'";
                tags.RemoveAt(0);
                foreach (var tagName in tags)
                {
                    command.CommandText += " OR tag.tag_name=" + "'" + tagName + "'";

                }
                command.CommandText += " ) AND tag.Ideas_idIdeas = idea.idIdeas AND state='Published'";
                var a = new DataConverter().GetDataTableFromSearchIdeaTags(command.ExecuteReader());
                return a;

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public bool AddTags(IIdeas aIdea, IList<string> aTagsList)
        {
            try
            {
                if (aTagsList.Count < 1)
                    return true;
                if (aIdea.Id == 0)
                    aIdea.Id = GetDb().GetLastIdeaId();

                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText = "INSERT INTO tag(Ideas_idIdeas,tag_name) VALUES";
                    foreach (var tagName in aTagsList)
                    {
                        command.CommandText += " ( " + aIdea.Id + ",'" + tagName + "'),";

                    }
                    command.CommandText = command.CommandText.Remove(command.CommandText.Length - 1);
                    command.ExecuteNonQuery();
                    return true;
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public bool DeleteTags(IIdeas aIdeas)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText = "DELETE FROM tag WHERE Ideas_idIdeas=@param1";
                    command.Parameters.AddWithValue("@param1", aIdeas.Id);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public IList<ITag> GetTags(IIdeas aIdea)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM tag WHERE Ideas_idIdeas=@param1";
                    command.Parameters.AddWithValue("@param1", aIdea.Id);
                    var dataReader = command.ExecuteReader();
                    return new DataConverter().GetListTagsFromIdea(dataReader);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }



        private bool voteExists(string aEmail, int aIdeaId)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM vote WHERE user_email=@param1 AND " +
                                          "idea_idIdeas=@param2";
                    command.Parameters.AddWithValue("@param1", aEmail);
                    command.Parameters.AddWithValue("@param2", aIdeaId);
                    var dataReader = command.ExecuteReader();
                    return dataReader.HasRows;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public bool Vote(string aEmail, int aIdeaId, int aVote)
        {
            try
            {
                using (var command = _mySqlConnection.CreateCommand())
                {
                    if (!voteExists(aEmail, aIdeaId))
                        command.CommandText =
                            "INSERT INTO vote(user_email,idea_idIdeas,vote) " +
                            "VALUES(@param1,@param2,@param3)";
                    else
                        command.CommandText =
                            "UPDATE vote SET vote=@param3 WHERE " +
                            "user_email=@param1 AND idea_idIdeas=@param2";

                    _mySqlConnection.Open();
                    command.Parameters.AddWithValue("@param1", aEmail);
                    command.Parameters.AddWithValue("@param2", aIdeaId);
                    command.Parameters.AddWithValue("@param3", aVote);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }

        }




        public IList<string> GetAllUserSubscription(string selfEmail)
        {
            try
            {
                _mySqlConnection.Open();
                var command = _mySqlConnection.CreateCommand();
                command.CommandText = "SELECT * FROM usersubscription WHERE subscriber=@param1";
                command.Parameters.AddWithValue("@param1", selfEmail);
                var converter = new DataConverter();
                return converter.GetEmailListFromUserSubs(command.ExecuteReader());
            }

            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }


        public bool SubscribeToUser(string selfEmail, string subEmail)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText =
                        "INSERT INTO usersubscription(subscriber,user,subscribed_time) " +
                        "VALUES(@param1,@param2,@param3)";
                    command.Parameters.AddWithValue("@param1", selfEmail);
                    command.Parameters.AddWithValue("@param2", subEmail);
                    command.Parameters.AddWithValue("@param3", DateTime.Now);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public bool UnSubscribeToUser(string selfEmail, string subEmail)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText = "DELETE FROM usersubscription WHERE subscriber=@param1 ";
                    command.CommandText += "AND user=@param2";
                    command.Parameters.AddWithValue("@param1", selfEmail);
                    command.Parameters.AddWithValue("@param2", subEmail);
                    command.ExecuteNonQuery();
                    return true;

                }
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public bool IsSubscribedToUser(string selfEmail, string subEmail)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText =
                        "SELECT * FROM usersubscription WHERE subscriber=@param2 AND user=@param1";
                    command.Parameters.AddWithValue("@param1", selfEmail);
                    command.Parameters.AddWithValue("@param2", subEmail);
                    return command.ExecuteReader().HasRows;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }


        public bool SubscribeToIdea(string selfEmail, int IdeaId)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText =
                        "INSERT INTO ideasubscription(user_email,idea_IdIdeas,subscribed_time) " +
                        "VALUES(@param1,@param2,@param3)";
                    command.Parameters.AddWithValue("@param1", selfEmail);
                    command.Parameters.AddWithValue("@param2", IdeaId);
                    command.Parameters.AddWithValue("@param3", DateTime.Now);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public bool UnSubscribeToIdea(string selfEmail, int IdeaId)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText = "DELETE FROM ideasubscription WHERE idea_idIdeas=@param1 ";
                    command.CommandText += "AND user_email=@param2";
                    command.Parameters.AddWithValue("@param1", IdeaId);
                    command.Parameters.AddWithValue("@param2", selfEmail);
                    command.ExecuteNonQuery();
                    return true;

                }
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public bool IsSubscribedToIdea(string SelfEmail, int IdeaId)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText =
                        "SELECT * FROM ideasubscription WHERE idea_idIdeas=@param2 AND user_email=@param1";
                    command.Parameters.AddWithValue("@param1", SelfEmail);
                    command.Parameters.AddWithValue("@param2", IdeaId);
                    return command.ExecuteReader().HasRows;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public bool AddComment(IComments aComment)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText =
                        "INSERT INTO comment(creator,idea,content,creation_date) " +
                        "VALUES(@param1,@param2,@param3,@param4)";
                    command.Parameters.AddWithValue("@param1", aComment.Creator);
                    command.Parameters.AddWithValue("@param2", aComment.Idea);
                    command.Parameters.AddWithValue("@param3", aComment.Content);
                    command.Parameters.AddWithValue("@param4", DateTime.Now);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public IList<IComments> GetComment(int ideiaId)
        {
            try
            {
                _mySqlConnection.Open();
                var command = _mySqlConnection.CreateCommand();
                command.CommandText = "SELECT * FROM comment WHERE idea=@param1 ORDER BY creation_date DESC";
                command.Parameters.AddWithValue("@param1", ideiaId);
                return new DataConverter().CommentsFromQuery(command.ExecuteReader());

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }

        }

        public long[] GetIdeaVoteCount(int aIdeaId)
        {
            try
            {
                long[] countVote = new long[2];
                _mySqlConnection.Open();
                var command = _mySqlConnection.CreateCommand();
                command.CommandText =
                    " select count(vote) as vote from vote where vote = 0 and vote.idea_idIdeas = @param1";

                command.Parameters.AddWithValue("@param1", aIdeaId);

                countVote[0] = new DataConverter().IdeaVoteCount(command.ExecuteReader());
                _mySqlConnection.Close();
                _mySqlConnection.Open();
                var command2 = _mySqlConnection.CreateCommand();

                command2.CommandText =
                    " select count(vote) as vote from vote where vote = 1 and vote.idea_idIdeas = @param1";
                command2.Parameters.AddWithValue("@param1", aIdeaId);

                countVote[1] = new DataConverter().IdeaVoteCount(command2.ExecuteReader());
                return countVote;

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public DataTable IdeaCommentsFeed(string email)
        {
            try
            {
                _mySqlConnection.Open();
                using (var command = _mySqlConnection.CreateCommand())
                {
                    command.CommandText =
                        "SELECT comment.creator, comment.idea, idea.title ,  comment.content, comment.creation_date " +
                        "FROM ideasubscription,comment, user, idea " +
                        "WHERE comment.creation_date > user.last_visit " +
                        "AND user.email = @param1 " +
                        "AND ideasubscription.user_email = @param1 " +
                        "AND ideasubscription.idea_idIdeas = comment.idea " +
                        "AND idea.idIdeas = comment.idea";
                    command.Parameters.AddWithValue("@param1", email);
                    return new DataConverter().FeedComments(command.ExecuteReader());
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }

        public DataTable IdeaUserFeed(string email)
        {
            try
            {
                _mySqlConnection.Open();
                var command = _mySqlConnection.CreateCommand();

                command.CommandText = "SELECT idea.idIdeas,idea.title,idea.creation_date,idea.owner " +
                                      "FROM ideasubscription, idea, user" +
                                      " WHERE ideasubscription.user_email = @param1 " +
                                      "AND idea.creation_date > user.last_visit " +
                                      "AND ideasubscription.idea_idIdeas = idea.idIdeas " +
                                      "AND ideasubscription.user_email = user.email " +
                                      "AND idea.state = @param2";
                command.Parameters.AddWithValue("@param1", email);
                command.Parameters.AddWithValue("@param2", "Published");
                return new DataConverter().FeedIdeas(command.ExecuteReader());

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }

        }

        public IList<dynamic> GetStatistics(string aEmail)
        {
            try
            {
                IList<dynamic> aList = new List<dynamic>();
                _mySqlConnection.Open();
                var command = _mySqlConnection.CreateCommand();
                command.CommandText = "SELECT " +
                                      "(select count(*) as total_coment from comment where comment.creator = @param1) as total_coment, " +
                                      "(select count(*) from idea where idea.owner = @param1) as total_ideas, " +
                                      "(select user.creation_date from user where user.email = @param1) as creation_date, " +
                                      "(select user.visits from user where user.email = @param1) as total_visits, " +
                                      "(select user.last_visit from user where user.email = @param1) as last_visit, " +
                                      "(select avg(vote.vote) from idea, vote where vote.idea_idIdeas = idea.idIdeas and idea.owner = " +
                                      "@param1) as rating";
                command.Parameters.AddWithValue("@param1", aEmail);
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    aList.Add((long) reader["total_coment"]);
                    aList.Add((long) reader["total_ideas"]);
                    aList.Add((DateTime) reader["creation_date"]);
                    aList.Add((DateTime) reader["last_visit"]);
                    aList.Add((int) reader["total_visits"]);
                    aList.Add(reader["rating"].GetType().Equals(typeof (DBNull)) ? 0: Convert.ToDecimal(reader["rating"]));
                    
                }
                return aList;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                _mySqlConnection.Close();
            }
        }
    }
}
