﻿using System;
using Interfaces;
using Interfaces.Statistic;

namespace Implementations.Statistics
{
    public class StatisticContainer : IStatisticContainer
    {

        public DateTime LastVisit { get; set; }
        public DateTime CreationDate { get; set; }
        public int TotalVisits { get; set; }
        public long TotalIdeas { get; set; }
        public long TotalComments { get; set; }
        public decimal UserRating { get; set; }
    }
}