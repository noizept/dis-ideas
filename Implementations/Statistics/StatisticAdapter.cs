﻿using System;
using System.Collections;
using System.Collections.Generic;
using Implementations.Database;
using Interfaces;
using Interfaces.Statistic;

namespace Implementations.Statistics
{
    public class StatisticAdapter : IStatisticsAdapter
    {
        private string _email;

        public StatisticAdapter(string aEmail)
        {
            _email = aEmail;
        }

        public IStatisticContainer GetStatistics()
        {
            return ListToObject(SQLDB.GetDb().GetStatistics(_email));
            
        }

        private IStatisticContainer ListToObject(IList<dynamic> aList)
        {
            IStatisticContainer aContainer = new StatisticContainer
            {
                TotalComments = (long) aList[0],
                TotalIdeas = (long) aList[1],
                CreationDate = (DateTime) aList[2],
                LastVisit = (DateTime) aList[3],
                TotalVisits = (int) aList[4],
                UserRating = (decimal) aList[5]
            };
            return aContainer;
        }
    }
}