﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Implementations.Status;
using Interfaces;

namespace Implementations
{

    public static class GeneralUtils
    {
        private enum MyEnum
        {
            
        }
        public static IStatus GetStatus(string aString)
        {
            IStatus aStatus;
            switch (aString)
            {
                case "Draft":
                    aStatus = new Draft();
                    break;
                case "Published":
                    aStatus = new Published();
                    break;
                default:
                    aStatus = new Draft();
                    break;
            }
            return aStatus;

        }
    }   
}
