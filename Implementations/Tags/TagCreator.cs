﻿using Interfaces;

namespace Implementations.Tags
{
    class TagCreator
    {
        public static ITag CreateTag(string aName)
        {
            ITag aTag=new Tag();
            aTag.TagName = aName;
            return aTag;
        }
        
    }
}
