﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Implementations.Tags;
using Interfaces;

namespace Implementations
{
    class Tag : ITag
    {
        public string TagName { get; set; }

    }
}
