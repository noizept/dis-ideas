﻿using System;
using Interfaces;

namespace Implementations.Comment
{
    public class Comment : IComments
    {
        public string Creator { get; set; }
        public int Idea { get; set; }
        public string Content { get; set; }
        public DateTime CreationTime { get; set; }
    }
}