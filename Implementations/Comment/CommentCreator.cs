﻿using Interfaces;

namespace Implementations.Comment
{
    public class CommentCreator
    {
        public static IComments CreateComment()
        {
            return new Comment();
        } 
    }
}