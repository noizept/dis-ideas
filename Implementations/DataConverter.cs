﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Implementations.Comment;
using Implementations.IdeasImplementation;
using Implementations.Tags;
using Implementations.UsersImplementation;
using Interfaces;
using Interfaces.Users;
using MySql.Data.MySqlClient;

namespace Implementations
{
    class DataConverter : IDataConverter
    {
        public DataTable GetIdeasTable(MySqlDataReader aData)
        {
            var listReader = new DataTable();
            listReader.Columns.Add("Id", typeof (int));
            listReader.Columns.Add("Title", typeof (string));
            listReader.Columns.Add("Creation", typeof (DateTime));
            listReader.Columns.Add("Status", typeof (string));

            while (aData.Read())
            {
                listReader.Rows.Add(
                    (int) aData["idIdeas"],
                    aData["title"] as string,
                    (DateTime) aData["creation_date"],
                    aData["state"] as string);
            }
            return listReader;
        }

        public Hashtable GetUserHashtable(MySqlDataReader dataReader)
        {
            var userInformation = new Hashtable();
            if (dataReader.HasRows)
            {
                while (dataReader.Read())
                {
                    userInformation.Add("email", dataReader["email"]);
                    userInformation.Add("first_name", dataReader["first_name"]);
                    userInformation.Add("last_name", dataReader["last_name"]);
                    userInformation.Add("acc_type", dataReader["acc_type"]);
                }
            }
            else
            {
                userInformation = null;
            }
            return userInformation;

        }

        public IIdeas GetIdea(MySqlDataReader dataReader)
        {
            IIdeas aIdea = IdeaCreator.CreateIdea();
            while (dataReader.Read())
            {
                aIdea.Id = (int) dataReader["idIdeas"];
                aIdea.Contennt = dataReader["content"] as string;
                aIdea.State = GeneralUtils.GetStatus(dataReader["state"] as string);
                aIdea.Title = dataReader["title"] as string;
            }
            return aIdea;
        }

        public string GetUserEmailByIdea(MySqlDataReader dataReader)
        {
            while (dataReader.Read())
            {
                return dataReader["owner"] as string;
            }
            return "";
        }

        public User GetUserObject(MySqlDataReader dataReader)
        {
            User aUser;
            while (dataReader.Read())
            {
                switch ((int) dataReader["acc_type"])
                {
                    case 1:
                        aUser = new Executive();
                        break;
                    case 2:
                        aUser = new Manager();
                        break;
                    case 3:
                        aUser = new Staff();
                        break;
                    default:
                        aUser = new Client();
                        break;

                }
                aUser.Email = dataReader["email"] as string;
                aUser.FirstName = dataReader["first_name"] as string;
                aUser.LastName = dataReader["last_name"] as string;
                aUser.Password = dataReader["password"] as string;
                return aUser;
            }
            return null;
        }

        public IList<ITag> GetListTagsFromIdea(MySqlDataReader dataReader)
        {
            var listReader = new List<ITag>();

            while (dataReader.Read())
            {
                var aTag = TagCreator.CreateTag(dataReader["tag_name"] as string);
                listReader.Add(aTag);
            }
            return listReader;
        }

 
        public DataTable GetUsersTable(MySqlDataReader aData)
        {
            var listReader = new DataTable();
            listReader.Columns.Add("Email", typeof(string));
            listReader.Columns.Add("Name", typeof(string));
            listReader.Columns.Add("Member Since", typeof(DateTime));
            listReader.Columns.Add("Subscribed", typeof(string));

            while (aData.Read())
            {
                 listReader.Rows.Add(
                    aData["email"] as string,
                    aData["first_name"] as string + " "+ aData["last_name"],
                    (DateTime)aData["creation_date"],
                    ""
                );
            }
            return listReader;
        }

        public IList<string> GetEmailListFromUserSubs(MySqlDataReader aDataReader)
        {
            IList<string> aList=new List<string>();
            if (aDataReader.HasRows)
            {
                while (aDataReader.Read())
                {
                    aList.Add(aDataReader["user"] as string);
                }
            }
            return aList;
        }

        public DataTable GetDataTableFromSearchIdeaTags(MySqlDataReader aData)
        {
            var listReader = new DataTable();
            listReader.Columns.Add("Id", typeof(int));
            listReader.Columns.Add("Title", typeof(string));
            listReader.Columns.Add("Creator", typeof (string));
            listReader.Columns.Add("Creation Date", typeof(DateTime));

            while (aData.Read())
            {

                listReader.Rows.Add(
                   (int) aData["idIdeas"],
                   aData["title"] as string,
                   aData["owner"] as string,
                   (DateTime)aData["creation_date"]
               );
            }
            return listReader;
        }

        public IList<IComments> CommentsFromQuery(MySqlDataReader aDataReader)
        {
            var listOfComments=new List<IComments>();
            while (aDataReader.Read())
            {
                IComments aComment = CommentCreator.CreateComment();
                aComment.Content = aDataReader["content"] as string;
                aComment.CreationTime = (DateTime) aDataReader["creation_date"];
                aComment.Creator = aDataReader["creator"] as string;
                listOfComments.Add(aComment);
            }
            return listOfComments;
        }

        public long IdeaVoteCount(MySqlDataReader aDataReader)
        {
            long x = 0;
            while (aDataReader.Read())
            {
                x = (long) aDataReader["vote"];
       
            }
            return x;
        }

        public DataTable FeedComments(MySqlDataReader aData)
        {
            var listReader = new DataTable();
            listReader.Columns.Add("Creator", typeof(string));
            listReader.Columns.Add("Comment", typeof(string));
            listReader.Columns.Add("Creation_Date", typeof(DateTime));
            listReader.Columns.Add("Idea_Title", typeof(string));
            listReader.Columns.Add("Idea_Id", typeof(int));
            while (aData.Read())
            {

                listReader.Rows.Add(
                   aData["creator"] as string,
                   aData["content"] as string,
                   (DateTime)aData["creation_date"],
                   aData["title"] as string,
                   (int)aData["idea"]
               );
            }
            return listReader;
        }

        public DataTable FeedIdeas(MySqlDataReader aData)
        {
            var listReader = new DataTable();
            listReader.Columns.Add("Idea_Id", typeof(int));
            listReader.Columns.Add("Title", typeof(string));
            listReader.Columns.Add("Creation_Date", typeof(DateTime));
            listReader.Columns.Add("owner", typeof(string));
            while (aData.Read())
            {
                listReader.Rows.Add(
                   (int)aData["idIdeas"],
                   aData["title"] as string,
                   (DateTime)aData["creation_date"],
                   aData["owner"] as string
               );
            }
            return listReader;
        }
    }
}
