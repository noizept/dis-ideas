﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces.Users;

namespace Interfaces
{
    interface IGroup
    {
        IList<Users.User> GetUsers();
        IList<IIdeas> GetIdeas();
        void InviteUser(Users.User aUser);
        void RemoveUser(Users.User aUser);

    }
}
