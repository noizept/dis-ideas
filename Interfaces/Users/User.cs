﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using Interfaces.Subsription;
using static System.Data.SqlClient.SqlConnection;

namespace Interfaces.Users
{
    public abstract class User
    {       
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

 
        public virtual string GetFullName()
        {
            return FirstName + " " + LastName;
        }

    }
}
