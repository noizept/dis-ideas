﻿using System.Data;

namespace Interfaces.DB
{
    public interface IDbIdeas
    {
        /// <summary>
        /// Retorna a Datatable com a pesquisa de tags
        /// </summary>
        /// <returns></returns>
        DataTable GetIdeasByTags(string ListTags);

        /// <summary>
        /// Método que recebendo um utilizador retorna as suas ideias
        /// </summary>
        /// <param name="aUser"></param>
        /// <returns>Lista de IDeias em formato DataTable</returns>
        DataTable GetIdeasDataTable(Users.User aUser);



        /// <summary>
        /// Insere uma nova ideia
        /// </summary>
        /// <param name="aIdeas"></param>
        /// <returns></returns>
        bool SaveIdea(IIdeas aIdeas);

        /// <summary>
        /// Modifica a informação de uma ideia
        /// </summary>
        /// <param name="aIdea"></param>
        /// <returns>true em caso de sucesso</returns>
        bool UpdateIdea(IIdeas aIdea);

        /// <summary>
        /// Metodo de eliminar ideias da BD
        /// </summary>
        /// <param name="aIdea"></param>
        /// <returns>true em caso de sucesso</returns>
        bool DeleteIdea(IIdeas aIdea);


        /// <summary>
        /// Metodo que retorna o email do owner de uma determinada ideia
        /// </summary>
        /// <param name="aId"></param>
        /// <returns></returns>
        string GetIdeaOwnerEmail(int aId);


        /// <summary>
        /// Ultimo id inserido na tabela de ideias
        /// </summary>
        /// <returns>retorna o id da ultima ideia</returns>
        int GetLastIdeaId();

        /// <summary>
        /// Retorna Ideia
        /// </summary>
        /// <param name="aId">Id da ideia</param>
        /// <returns>Retorna objeto de Ideia </returns>
        IIdeas GetIdea(int aId);
    }
}