﻿using System.Collections;
using System.Collections.Generic;
using Interfaces.Users;

namespace Interfaces.DB
{
    public interface ILogin
    {
        // =================== USERS ======================= //
        /// <summary>
        /// Método de registo de utilizador na base de dados
        /// </summary>
        /// <param name="aDictionary"></param>
        /// <returns>Retorna true em caso de sucesso</returns>
        bool RegistUser(Hashtable aDictionary);


        /// <summary>
        /// Método de remover utilizadores da base de dados
        /// </summary>
        /// <param name="email"></param>
        /// <returns>retorna true em caso de sucesso</returns>
        bool DeleteUser(string email);

        /// <summary>
        ///  Retorna o objecto User que criou a ideia com um determinado ID
        /// </summary>
        /// <param name="aId"></param>
        /// <returns></returns>
        User GetUserByEmail(string aEmail);

        /// <summary>
        /// Altera a data da ultima visita do utilizador
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        bool UpdateLastVisit(string email);

        /// <summary>
        /// Incrementa o numero de visitas do utilizador
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        bool IncrementVisits(string email);

    }
}