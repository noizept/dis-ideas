﻿using System.Collections.Generic;

namespace Interfaces.DB
{
    public interface IDbSubscription
    {
        /// <summary>
        /// Retorna lista de todos os emails a qual está subscrito
        /// </summary>
        /// <param name="selfEmail"></param>
        /// <returns></returns>
        IList<string> GetAllUserSubscription(string selfEmail);

        /// <summary>
        /// Metodo para guardar na BD a subscripção do utilizador
        /// </summary>
        /// <param name="selfEmail"></param>
        /// <param name="subEmail"></param>
        /// <returns></returns>
        bool SubscribeToUser(string selfEmail, string subEmail);

        /// <summary>
        /// Metodo para remover na BD a subscripção do utilizador
        /// </summary>
        /// <param name="selfEmail"></param>
        /// <param name="subEmail"></param>
        /// <returns></returns>
        bool UnSubscribeToUser(string selfEmail, string subEmail);

        /// <summary>
        /// Verifica se o utilizador está subscripto
        /// </summary>
        /// <param name="selfEmail"></param>
        /// <param name="subEmail"></param>
        /// <returns></returns>
        bool IsSubscribedToUser(string selfEmail, string subEmail);

        /// <summary>
        /// Metodo para guardar na BD a subscripção da ideia
        /// </summary>
        /// <param name="selfEmail"></param>
        /// <param name="subEmail"></param>
        /// <returns></returns>
        bool SubscribeToIdea(string selfEmail, int IdeaId);

        /// <summary>
        /// Metodo para remover na BD a subscripção do ideia
        /// </summary>
        /// <param name="selfEmail"></param>
        /// <param name="subEmail"></param>
        /// <returns></returns>
        bool UnSubscribeToIdea(string selfEmail, int IdeaId);

        /// <summary>
        /// Verifica se o utilizador está subscripto
        /// </summary>
        /// <param name="selfEmail"></param>
        /// <param name="subEmail"></param>
        /// <returns></returns>
        bool IsSubscribedToIdea(string SelfEmail, int IdeaId);
    }
}