﻿using System.Data;

namespace Interfaces.DB
{
    public interface IDbFeed
    {
        //===============FEED ================//

        /// <summary>
        /// Feed de coments nas ideias subescritas
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        DataTable IdeaCommentsFeed(string email);

        /// <summary>
        /// Feed de novas ideias dos utilizadores subescritos
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        DataTable IdeaUserFeed(string email);
    }
}