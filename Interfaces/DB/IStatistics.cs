﻿using System.Collections.Generic;

namespace Interfaces.DB
{
    public interface IStatistics
    {
        /// <summary>
        /// Metodo que retorna os dados estatisticos
        /// </summary>
        /// <param name="aEmail"></param>
        /// <returns></returns>
        IList<dynamic> GetStatistics(string aEmail);
    }
}