﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IComments
    {
        string Creator { get; set; }
        int Idea { get; set; }
        string Content { get; set; }
        DateTime CreationTime { get; set; }

    }
}
