﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces.Users;
using MySql.Data.MySqlClient;

namespace Interfaces
{
    public interface IDataConverter
    {
        /// <summary>
        /// Converte uma Pesquisa da BD em uma DataTableble
        /// </summary>
        /// <param name="aData"></param>
        /// <returns></returns>
        DataTable GetIdeasTable(MySqlDataReader aData);

        /// <summary>
        /// Converte uma Pesquisa da BD em uma HashTable
        /// </summary>
        /// <param name="aData"></param>
        /// <returns></returns>
        Hashtable GetUserHashtable(MySqlDataReader aData);

        /// <summary>
        /// Converte uma pesquisa da BD em um objecto do tipo Idea
        /// </summary>
        /// <param name="aId"></param>
        /// <returns></returns>
        IIdeas GetIdea(MySqlDataReader aDataReader);


        /// <summary>
        /// Converte uma pesquisa da BD, retornando uma string com o email
        /// </summary>
        /// <param name="aDataReader"></param>
        /// <returns></returns>
        string GetUserEmailByIdea(MySqlDataReader aDataReader);

        /// <summary>
        /// Converte uma query a BD em um objeto do tipo USER
        /// </summary>
        /// <param name="aDataReader"></param>
        /// <returns></returns>
        User GetUserObject(MySqlDataReader aDataReader);

        /// <summary>
        /// Converte uma query da BD em uma lista de Itags
        /// </summary>
        /// <param name="aDataReader"></param>
        /// <returns></returns>
        IList<ITag> GetListTagsFromIdea(MySqlDataReader aDataReader);

        /// <summary>
        /// Converte uma query da BD em uma Datatable
        /// </summary>
        /// <param name="aDataReader"></param>
        /// <returns></returns>
        DataTable GetUsersTable(MySqlDataReader aDataReader);

        /// <summary>
        /// Converte uma query BD em uma lista de emails
        /// </summary>
        /// <param name="aDataReader"></param>
        /// <returns></returns>
        IList<string> GetEmailListFromUserSubs(MySqlDataReader aDataReader);

        /// <summary>
        /// Converte uma query na BD em uma datatable
        /// </summary>
        /// <param name="aDataReader"></param>
        /// <returns></returns>
        DataTable GetDataTableFromSearchIdeaTags(MySqlDataReader aDataReader);


        /// <summary>
        /// Converte query BD em uma lista de comentarios
        /// </summary>
        /// <param name="aDataReader"></param>
        /// <returns></returns>
        IList<IComments> CommentsFromQuery(MySqlDataReader aDataReader);

        /// <summary>
        /// Converte query BD em um array com os numeros de votos
        /// </summary>
        /// <param name="aDataReader"></param>
        /// <returns></returns>
        long IdeaVoteCount(MySqlDataReader aDataReader);


        /// <summary>
        /// Converte uma BD em um feed de comments
        /// </summary>
        /// <param name="aDataReader"></param>
        /// <returns></returns>
        DataTable FeedComments(MySqlDataReader aDataReader);

        /// <summary>
        /// Converte uma BD em um feed de Ideias
        /// </summary>
        /// <param name="aData"></param>
        /// <returns></returns>
        DataTable FeedIdeas(MySqlDataReader aData);
    }
}
