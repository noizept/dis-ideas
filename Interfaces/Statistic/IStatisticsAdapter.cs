﻿namespace Interfaces.Statistic
{
    public interface IStatisticsAdapter
    {
        IStatisticContainer GetStatistics();
    }
}