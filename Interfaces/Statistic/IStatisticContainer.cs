﻿using System;

namespace Interfaces.Statistic
{
    public interface IStatisticContainer
    {
        DateTime LastVisit { get; set; }
        DateTime CreationDate { get; set; }
        int TotalVisits { get; set; }
        long TotalIdeas { get; set; }
        long TotalComments { get; set; }
        decimal UserRating { get; set; }

    }
}