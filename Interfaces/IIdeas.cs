﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Interfaces.Users;

namespace Interfaces
{
    public interface IIdeas
    {
        string Title { get; set; }
        string Contennt { get; set; }
        IStatus State { get; set; }
        User Owner { get; set; }
        DateTime CreaDateTime { get; set; }
        int Id { get; set; }
        IList<ITag> Tags { get; set; } 

    }
}