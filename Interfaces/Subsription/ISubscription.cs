﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces.Subsription
{
    
    /// <summary>
    /// Interface de Subscrição
    /// </summary>
    public interface ISubscription
    {
        bool Execute(string aSubscriptionEmail);
    }
}
