﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Interfaces.Users;
using MySql.Data.MySqlClient;

namespace Interfaces
{
    public interface IDatabase
    {

        /// <summary>
        /// Metodo que retorna a listagem completa de utilizadores registados
        /// </summary>
        /// <returns></returns>
        DataTable GetAllUsersDataTable();

        //============================= TAGS ===========================//

        /// <summary>
        /// Metodo que adiciona as Tagas na BD com a ideia associada
        /// </summary>
        /// <param name="aIdea"></param>
        /// <param name="aTagslist"></param>
        /// <returns>retorna true se sucedido</returns>
        bool AddTags(IIdeas aIdea,IList<string> aTagslist);

        /// <summary>
        /// Remove todas as tags de uma determinada Ideia
        /// </summary>
        /// <param name="aIdeas"></param>
        /// <returns></returns>
        bool DeleteTags(IIdeas aIdeas);

        /// <summary>
        /// Retorna a lista das tagas que uma certa ideia contem
        /// </summary>
        /// <param name="aIdea"></param>
        /// <returns></returns>
        IList<ITag> GetTags(IIdeas aIdea);



        //========================= VOTE ========================= //
        /// <summary>
        /// Metodo de voto 
        /// </summary>
        /// <param name="aEmail"></param>
        /// <param name="aIdeaId"></param>
        /// <param name="aVote"></param>
        /// <returns>true em caso de sucesso</returns>
        bool Vote(string aEmail, int aIdeaId, int aVote);



        //================ COMMENTS ==========================//

        /// <summary>
            /// Metodo que insere comentário na BD
            /// </summary>
            /// <param name="aCreator"></param>
            /// <param name="IdeaId"></param>
            /// <param name="Content"></param>
            /// <returns></returns>
        bool AddComment(IComments aComment);

        /// <summary>
        /// Retorna todos os comentários de uma ideia
        /// </summary>
        /// <param name="ideiaId"></param>
        /// <returns></returns>
        IList<IComments> GetComment(int ideiaId);


        //===============================VOTE =============///
        /// <summary>
        /// Retorna contagem dos votos positivos e negativos
        /// </summary>
        /// <param name="aIdeaId"></param>
        /// <returns></returns>
        long[] GetIdeaVoteCount(int aIdeaId);


    }
}
