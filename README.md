# README #

*Projeto DIS 2015-2016 Ideial

### What is this repository for? ###
* Disciplina Desenho Implementação de Software
* Pedro Campos
* Projeto respeitando principios SOLID e aplicação Padrões de desenho
* Version 1.0

### How do I get set up? ###

* Configuration : C#
* Dependencies : 
* * Mysql.data
* * Mahapps
* * Material Design XAML

* Database configuration
* * MySQL

### Who do I talk to? ###

* Nino Velosa or Sérgio Viúla
* ninovelosa@gmail.com / sergioviula@gmail.com