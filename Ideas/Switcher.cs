﻿using System.Windows.Controls;

namespace Ideas
{
    public class Switcher
    {
        public static mainwindow Mainwindow;

        public static void Switch(UserControl newPage)
        {
            Mainwindow.Navigate(newPage);
        }

        public static void Switch(UserControl newPage, object state)
        {
           Mainwindow.Navigate(newPage,state);            
        }
    }
}