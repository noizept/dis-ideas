﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ideas.Views.Search;
using Implementations.Comment;
using Implementations.Database;
using Implementations.Rating;
using Interfaces;
using Interfaces.Users;
using MaterialDesignThemes.Wpf;

namespace Ideas.Views
{
    /// <summary>
    /// Interaction logic for IdeaDisplayWithComments.xaml
    /// </summary>
    public partial class IdeaDisplayWithComments : UserControl, ISwitchable
    {
        private User _mLoggedUser;
        private IList<IComments> _mComments;
        private IIdeas _mIdea;
        private long[] _mVotes;
        private VoteInvoker voteInvoker;
        private IRating upRating;
        private IRating downRating;
        private TextBox _mCommentTextBox;
        private bool _mIdeaSubStatus;

        public IdeaDisplayWithComments()
        {
            InitializeComponent();
            _mCommentTextBox = new TextBox
            {    
                MinWidth = 200,
                MaxWidth = 600,
                Margin = new Thickness(0,0,15,15),
                TextWrapping = TextWrapping.Wrap,
                AcceptsReturn = true,
                Name = "commentInput",
                Text = ""
            };
        }

        public void UtilizeState(object state)
        {
            IDictionary<string, dynamic> passedState = state as IDictionary<string, dynamic>;
            if (passedState != null)
            {
                _mLoggedUser = passedState["User"];
                _mComments = passedState["Comments"];
                _mIdea = passedState["Idea"];
                _mVotes = passedState["Votes"];
                _mIdeaSubStatus = passedState["Sub"];
            }

            upRating = new VoteUpCommand(_mLoggedUser.Email);
            downRating = new VoteDownCommand(_mLoggedUser.Email);
            voteInvoker = new VoteInvoker(upRating,downRating);

            TextBlockName.Text=UiHelper.GetUiHelper().ReturnFullName(_mLoggedUser);

            StackPanel stackPanelTitle = new StackPanel {Orientation = Orientation.Horizontal};
            Label titleLabel = new Label
            {
                Margin = new Thickness(45, 15, 0, 0),
                FontSize = 16,
                Content = "Title: "
            };
            TextBlock ideaTile = new TextBlock
            {
                Text = _mIdea.Title,
                FontSize = 20,
                Foreground = new SolidColorBrush(Colors.BlueViolet),
                Margin = new Thickness(0, 17, 0, 0)
            };
            TextBlock votesBlock = new TextBlock
            {
                Text = "Vote for this Idea!",
                Margin = new Thickness(200,17,0,0)
            };

            StackPanel ideaContentStackPanel = new StackPanel();
            ideaContentStackPanel.Orientation = Orientation.Horizontal;
            Label ideaContentLabel = new Label
            {
                Content = "Content: ",
                FontSize = 16,
                Margin = new Thickness(45, 15, 0, 0)
            };
            TextBlock ideaContentTextBlock = new TextBlock
            {
                Text = _mIdea.Contennt,
                Margin = new Thickness(0, 17, 0, 0),
                FontSize = 20
            };

            Border ideaBg = new Border
            {
                Background = new SolidColorBrush(Colors.Gainsboro),
                CornerRadius = new CornerRadius(5),
                Padding = new Thickness(5),
                MinWidth = 250,
                MaxWidth = 850
            };
            ideaBg.Child = ideaContentTextBlock;

            #region Images to vote up or down
            StackPanel imagesPanel = new StackPanel
            {
                Orientation = Orientation.Vertical
            };
            StackPanel imagesDisplayPanel = new StackPanel
            {
                Orientation = Orientation.Horizontal
            };
            var uriUp = new Uri("pack://application:,,,/Views/Resources/like.png");
            var imgUp = new BitmapImage(uriUp);
            Image upVoteImage = new Image
            {
                Source = imgUp,
                Width = 45,
                Height = 45,
                Margin = new Thickness(45,0,0,0),
                Cursor = Cursors.Hand                
            };
            upVoteImage.MouseLeftButtonDown += UpVoteImageOnMouseLeftButtonDown;
            
            var uriDown = new Uri("pack://application:,,,/Views/Resources/unlike.png");
            var imgDown = new BitmapImage(uriDown);
            Image downVoteImage = new Image
            {
                Source = imgDown,
                Width = 45,
                Height = 45,
                Margin = new Thickness(45, 0, 0, 0),
                Cursor = Cursors.Hand

            };
            downVoteImage.MouseLeftButtonDown += DownVoteImageOnMouseLeftButtonDown;

            StackPanel imagesInfoCountPanel = new StackPanel
            {
                Orientation = Orientation.Horizontal
            };
            TextBlock likes = new TextBlock
            {
                Text = _mVotes[1].ToString() ?? "0",
                Margin = new Thickness(60,0,0,0)

            };
            TextBlock unlikes = new TextBlock
            {
                Text = _mVotes[0].ToString() ?? "0",
                Margin = new Thickness(70,0,0,0)                
            };

            Button subScibeBtn = new Button
            {
                Content = (_mIdeaSubStatus ? "Unsubscribe from Idea" : "Subscribe to Idea"),
                Width = 200,
                Margin = new Thickness(150,0,0,0)
            };
            subScibeBtn.Click +=SubScibeBtnOnClick;

            imagesInfoCountPanel.Children.Add(likes);
            imagesInfoCountPanel.Children.Add(unlikes);

            imagesDisplayPanel.Children.Add(upVoteImage);
            imagesDisplayPanel.Children.Add(downVoteImage);
            imagesDisplayPanel.Children.Add(subScibeBtn);


            imagesPanel.Children.Add(imagesDisplayPanel);
            imagesPanel.Children.Add(imagesInfoCountPanel);

           

#endregion

            StackPanel ideaContentFooter = new StackPanel {Orientation = Orientation.Horizontal};
            TextBlock ideaCreationDate = new TextBlock
            {
                Text = "Created at: " + _mIdea.CreaDateTime,
                Margin = new Thickness(47, 17, 0, 0)
            };
            ideaContentFooter.Children.Add(ideaCreationDate);

            //comment expander
            Expander commentsExpander = new Expander
            {
                Header = "Comments",
                Margin = new Thickness(15, 20, 0, 0),
                FontSize = 14
            };
            StackPanel commentsStackPanel = new StackPanel
            {
                Orientation = Orientation.Vertical
                
            };

            //Add new comment
            StackPanel addCommentStackPanel = new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Margin = new Thickness(47, 40, 0, 0)
            };

            Label newCommentLabel = new Label
            {
                Content = "Leave Feedback: ",
                FontSize = 18,
                Margin = new Thickness(0,0,15,0)
            };

            Button addCommentButton = new Button
            {
                Content = "Add new Comment",
                Background = new SolidColorBrush(Colors.BlueViolet),
                Foreground = new SolidColorBrush(Colors.White),
                Width = 170,                
            };
            addCommentButton.Click += AddCommentButtonOnClick;

            addCommentStackPanel.Children.Add(newCommentLabel);
            addCommentStackPanel.Children.Add(_mCommentTextBox);
            addCommentStackPanel.Children.Add(addCommentButton);

            #region commentbody
            StackPanel comments = new StackPanel();
            foreach (var comment in _mComments)
            {                
                StackPanel commnetHeader = new StackPanel {Orientation = Orientation.Horizontal};
                TextBlock creatorBlock = new TextBlock {Text = "Comment by "};
                creatorBlock.Margin = new Thickness(0,27,0,0);
                TextBlock commentBlock = new TextBlock {Text = comment.Creator};
                commentBlock.FontWeight = FontWeights.Bold;
                commentBlock.Foreground = new SolidColorBrush(Colors.BlueViolet);
                commentBlock.Margin = new Thickness(0,27,0,0);
                

                commnetHeader.Children.Add(creatorBlock);
                commnetHeader.Children.Add(commentBlock);
                
                StackPanel commentBody = new StackPanel { Orientation = Orientation.Horizontal};

                Border commentBg = new Border
                {
                    Background = new SolidColorBrush(Colors.Gainsboro),
                    CornerRadius = new CornerRadius(5),
                    Padding = new Thickness(5),
                    MinWidth = 250,
                    MaxWidth = 850
                };
                TextBlock commentContent = new TextBlock { Text = comment.Content};
                commentContent.Margin = new Thickness(10,10,0,10);
                commentContent.FontSize = 18;
                commentBg.Child = commentContent;
                commentBody.Children.Add(commentBg);

                StackPanel commentFooter = new StackPanel {Orientation = Orientation.Horizontal};               
                TextBlock createdAt = new TextBlock { Text = "Created at: " + comment.CreationTime };
                commentFooter.Children.Add(createdAt);

                comments.Children.Add(commnetHeader);
                comments.Children.Add(commentBody);
                comments.Children.Add(commentFooter);

            }
#endregion

            commentsStackPanel.Children.Add(comments);           
            commentsExpander.Content = commentsStackPanel;
            
            stackPanelTitle.Children.Add(titleLabel);
            stackPanelTitle.Children.Add(ideaTile);
            stackPanelTitle.Children.Add(votesBlock);

            ideaContentStackPanel.Children.Add(ideaContentLabel);
            ideaContentStackPanel.Children.Add(ideaBg);
            ideaContentStackPanel.Children.Add(imagesPanel);

            IdeaContent.Children.Add(stackPanelTitle);
            IdeaContent.Children.Add(ideaContentStackPanel);
            IdeaContent.Children.Add(ideaContentFooter);
            IdeaContent.Children.Add(addCommentStackPanel);
            IdeaContent.Children.Add(commentsExpander);


        }

        private void SubScibeBtnOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (_mIdeaSubStatus)
            {
                SQLDB.GetDb().UnSubscribeToIdea(_mLoggedUser.Email, _mIdea.Id);
            }
            else
            {
                SQLDB.GetDb().SubscribeToIdea(_mLoggedUser.Email, _mIdea.Id);
            }
            ReopenPage();
        }

        private void AddCommentButtonOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (!string.IsNullOrEmpty(_mCommentTextBox.Text))
            {
                IComments comment = CommentCreator.CreateComment();
                comment.Content = _mCommentTextBox.Text;
                comment.Creator = _mLoggedUser.Email;
                comment.Idea = _mIdea.Id;
                SQLDB.GetDb().AddComment(comment);

                this.ReopenPage();
            }
        }

        private void DownVoteImageOnMouseLeftButtonDown(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            voteInvoker.DownVote(_mIdea.Id);
            this.ReopenPage();
        }

        private void UpVoteImageOnMouseLeftButtonDown(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            voteInvoker.UpVote(_mIdea.Id);
            this.ReopenPage();
        }

        private void Profile_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new PersonalPage(), _mLoggedUser);
        }

        private void NewIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new NewIdea(), _mLoggedUser);
        }

        private void EditIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //TODO async            
            Switcher.Switch(new EditIdeas(), UiHelper.GetUiHelper().GetIdeaListDictionary(_mLoggedUser));
        }


        private void UserListBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new UserList(), UiHelper.GetUiHelper().GetUserListDictionary(_mLoggedUser));
        }

        private void GroupsBtn_OnClick(object sender, RoutedEventArgs e)
        {
            //Switcher.Switch(new Group);
        }

        private void Search_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (SearchTextBox.Text != null)
            {
                IDictionary<string, dynamic> stateObjects = new Dictionary<string, dynamic>();
                stateObjects.Add("User", _mLoggedUser);
                stateObjects.Add("SearchResults", SQLDB.GetDb().GetIdeasByTags(SearchTextBox.Text.ToUpper()));
                Switcher.Switch(new SearchResult(), stateObjects);
            }

        }
        private void ReopenPage()
        {
            IDictionary<string, dynamic> stateObjects = new Dictionary<string, dynamic>();
            stateObjects.Add("User", _mLoggedUser);
            stateObjects.Add("Idea", _mIdea);

            var comments = SQLDB.GetDb().GetComment(_mIdea.Id);
            var totalVotes = SQLDB.GetDb().GetIdeaVoteCount(_mIdea.Id);
            var subScibed = SQLDB.GetDb().IsSubscribedToIdea(_mLoggedUser.Email,_mIdea.Id);

            stateObjects.Add("Comments", comments);
            stateObjects.Add("Votes", totalVotes);
            stateObjects.Add("Sub", subScibed);

            Switcher.Switch(new IdeaDisplayWithComments(), stateObjects);
        }

        private void FeedBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new mainWindow(), _mLoggedUser);
        }
    }
}
