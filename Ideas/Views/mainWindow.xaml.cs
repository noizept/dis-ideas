﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Ideas.Views.Search;
using Implementations.Database;
using Implementations.Statistics;
using Interfaces.Users;
using MahApps.Metro.Controls;
using MaterialDesignThemes.Wpf;

namespace Ideas.Views
{
    /// <summary>
    /// Interaction logic for mainWindow.xaml
    /// </summary>
    public partial class mainWindow : ISwitchable
    {
        private User _mLoggedUser;

        public mainWindow()
        {
            InitializeComponent();
        }

        public void UtilizeState(object state)
        {
            var loggedUser = state as User;
            if (loggedUser != null)
            {
                _mLoggedUser = loggedUser;
                TextBlockName.Text = UiHelper.GetUiHelper().ReturnFullName(_mLoggedUser);
            }
            NewPostedComments.ItemsSource = SQLDB.GetDb().IdeaCommentsFeed(_mLoggedUser.Email).DefaultView;
            NewPostedIdeas.ItemsSource = SQLDB.GetDb().IdeaUserFeed(_mLoggedUser.Email).DefaultView;

            SQLDB.GetDb().UpdateLastVisit(_mLoggedUser.Email);
        }

        private void NewIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new NewIdea(), _mLoggedUser);
        }

        private void EditIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //TODO async            
            Switcher.Switch(new EditIdeas(), UiHelper.GetUiHelper().GetIdeaListDictionary(_mLoggedUser));
        }

        private void UserListBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new UserList(), UiHelper.GetUiHelper().GetUserListDictionary(_mLoggedUser));
        }

        private void GroupsBtn_OnClick(object sender, RoutedEventArgs e)
        {
            //Switcher.Switch(new Group);
        }

        private void Search_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (SearchTextBox.Text != null)
            {
                IDictionary<string, dynamic> stateObjects = new Dictionary<string, dynamic>();
                stateObjects.Add("User", _mLoggedUser);
                stateObjects.Add("SearchResults", SQLDB.GetDb().GetIdeasByTags(SearchTextBox.Text.ToUpper()));
                Switcher.Switch(new SearchResult(),stateObjects);
            }

        }

        private void NewPostedComments_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var row = ItemsControl.ContainerFromElement((DataGrid)sender, e.OriginalSource as DependencyObject) as DataGridRow;
            var data = row.Item;
            var properties = TypeDescriptor.GetProperties(data);
            var property = properties["Idea_Id"];
            OnRowClickDisplayIdea(data,property);

        }

        private void OnRowClickDisplayIdea(object data, PropertyDescriptor property)
        {
            var comments = SQLDB.GetDb().GetComment(Convert.ToInt32(property.GetValue(data).ToString()));
            var idea = SQLDB.GetDb().GetIdea(Convert.ToInt32(property.GetValue(data).ToString()));
            var totalVotes = SQLDB.GetDb().GetIdeaVoteCount(Convert.ToInt32(property.GetValue(data).ToString()));
            var subScibed = SQLDB.GetDb().IsSubscribedToIdea(_mLoggedUser.Email, Convert.ToInt32(property.GetValue(data).ToString()));
            IDictionary<string, dynamic> stateToPass = new Dictionary<string, dynamic>();
            stateToPass.Add("User", _mLoggedUser);
            stateToPass.Add("Idea", idea);
            stateToPass.Add("Comments", comments);
            stateToPass.Add("Votes", totalVotes);
            stateToPass.Add("Sub", subScibed);
            Switcher.Switch(new IdeaDisplayWithComments(), stateToPass);
        }
        private void Profile_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var aUserStats = (new StatisticAdapter(_mLoggedUser.Email)).GetStatistics();
            IDictionary<string, dynamic> stateObjects = new Dictionary<string, dynamic>();
            stateObjects.Add("User", _mLoggedUser);
            stateObjects.Add("Stats", aUserStats);

            Switcher.Switch(new PersonalPage(), stateObjects);
        }
    }
}
