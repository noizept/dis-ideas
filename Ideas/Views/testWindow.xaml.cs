﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Implementations.Database;
using MaterialDesignThemes.Wpf;

namespace Ideas.Views
{
    /// <summary>
    /// Interaction logic for testWindow.xaml
    /// </summary>
    public partial class TestWindow : UserControl
    {
        private readonly BackgroundWorker worker = new BackgroundWorker();
        public TestWindow()
        {
            InitializeComponent();
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.WorkerSupportsCancellation = true;

        }

        private async void worker_DoWork(object sender, DoWorkEventArgs e)
        {
           
        }

        private async void worker_RunWorkerCompleted(object sender,RunWorkerCompletedEventArgs e)
        {
            await Task.Delay(1000);
            if (!e.Cancelled)
            {              
                var user = (Hashtable) e.Result;
                if (user != null)
                {
                    Switcher.Switch(new mainWindow(), user);
                }
            }
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            string[] detailsStrings = new[] {"merda", "123456"};
            worker.RunWorkerAsync(detailsStrings);
        }

        private void OnDialogClosingLoginDialog(object sender, DialogClosingEventArgs eventargs)
        {
            worker.CancelAsync();
        }
    }
}
