﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Implementations.Database;
using Interfaces.Users;

namespace Ideas.Views.Search
{
    /// <summary>
    /// Interaction logic for SearchResult.xaml
    /// </summary>
    public partial class SearchResult : UserControl,ISwitchable
    {
        private User _mLoggedUser;
        public SearchResult()
        {
            InitializeComponent();
        }

        private void Search_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (SearchTextBox.Text != null)
            {
                IDictionary<string, dynamic> stateObjects = new Dictionary<string, dynamic>();
                stateObjects.Add("User", _mLoggedUser);
                stateObjects.Add("SearchResults", SQLDB.GetDb().GetIdeasByTags(SearchTextBox.Text.ToUpper()));
                Switcher.Switch(new SearchResult(), stateObjects);
            }

        }

        private void GroupsBtn_OnClick(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void UserListBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new UserList(), UiHelper.GetUiHelper().GetUserListDictionary(_mLoggedUser));
        }

        private void NewIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new NewIdea(), _mLoggedUser);
        }

        private void EditIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //TODO async            
            Switcher.Switch(new EditIdeas(), UiHelper.GetUiHelper().GetIdeaListDictionary(_mLoggedUser));
        }

        private void Profile_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new PersonalPage(), _mLoggedUser);
        }

        public void UtilizeState(object state)
        {
            var passedState = state as IDictionary<string, dynamic>;
            _mLoggedUser = passedState["User"];
            TextBlockName.Text = UiHelper.GetUiHelper().ReturnFullName(_mLoggedUser);
            SearchGrid.ItemsSource = ((DataTable) passedState["SearchResults"]).DefaultView;
        }

        private void SearchGrid_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {            
            var row = ItemsControl.ContainerFromElement((DataGrid)sender, e.OriginalSource as DependencyObject) as DataGridRow;
            var data = row.Item;            
            var properties = TypeDescriptor.GetProperties(data);
            var property = properties["Id"];           
            var comments = SQLDB.GetDb().GetComment(Convert.ToInt32(property.GetValue(data).ToString()));
            var idea = SQLDB.GetDb().GetIdea(Convert.ToInt32(property.GetValue(data).ToString()));
            var totalVotes = SQLDB.GetDb().GetIdeaVoteCount(Convert.ToInt32(property.GetValue(data).ToString()));
            var subScibed = SQLDB.GetDb().IsSubscribedToIdea(_mLoggedUser.Email,Convert.ToInt32(property.GetValue(data).ToString()));
            IDictionary<string,dynamic> stateToPass = new Dictionary<string, dynamic>();
            stateToPass.Add("User",_mLoggedUser);
            stateToPass.Add("Idea",idea);
            stateToPass.Add("Comments",comments);
            stateToPass.Add("Votes",totalVotes);
            stateToPass.Add("Sub",subScibed);
            Switcher.Switch(new IdeaDisplayWithComments(),stateToPass);
        }

        private void FeedBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new mainWindow(),_mLoggedUser);
        }
    }
}
