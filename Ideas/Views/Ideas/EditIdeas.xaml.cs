﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Implementations.Database;
using Interfaces;
using Interfaces.Users;

namespace Ideas.Views
{    
    /// <summary>
    /// Interaction logic for EditIdeas.xaml
    /// </summary>
    public partial class EditIdeas : UserControl,ISwitchable
    {
        private DataTable _mIdeas;
        private User _mLoggedUser;
        public EditIdeas()
        {
            InitializeComponent();
        }

        public void UtilizeState(object state)
        {
            var s = state as IDictionary<string,dynamic>;
            _mLoggedUser = s["User"];
            _mIdeas = s["Ideas"];
            PopulateDataGrid(_mIdeas);
            if (_mIdeas != null) TextBlockName.Text = _mLoggedUser.FirstName + " " + _mLoggedUser.LastName;
        }

        private void TextBlockName_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {            
            Switcher.Switch(new PersonalPage(),_mLoggedUser);
        }

        private void PopulateDataGrid(DataTable dt)
        {
           /* DataTable dataToPopulate = new DataTable("Ideas");
            dataToPopulate.Columns.Add("ID", typeof (int));
            dataToPopulate.Columns.Add("Title", typeof (string));
            dataToPopulate.Columns.Add("Creation Date", typeof (DateTime));
            dataToPopulate.Columns.Add("State", typeof(string));

            foreach (var idea in _mIdeas)
            {
                dataToPopulate.Rows.Add(idea.Id,idea.Title,idea.CreaDateTime,idea.State.GetStatus());
            }
            */
            IdeasGrid.ItemsSource = dt.DefaultView;
            //TODO if empty?
        }

        private void IdeasGrid_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs eArgs)
        {
            try
            {
                DataGridRow row = ItemsControl.ContainerFromElement((DataGrid)sender, eArgs.OriginalSource as DependencyObject) as DataGridRow;
                object data = row.Item;
                // extract the property value
                PropertyDescriptorCollection properties =
                    TypeDescriptor.GetProperties(data);
                PropertyDescriptor property = properties["Id"];
                //TODO async
                IIdeas aIdea = SQLDB.GetDb().GetIdea(Convert.ToInt32(property.GetValue(data)));
                IList<ITag> tags = SQLDB.GetDb().GetTags(aIdea);
                aIdea.Owner = _mLoggedUser;
                aIdea.Tags = tags;
                Switcher.Switch(new EditIdea(),aIdea);
            }
            catch (Exception e)
            {                
                throw new Exception(e.Message, e);
            }                      
            //TODO pass if of idea to new usercontrol
        }

        private void NewIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new NewIdea(),_mLoggedUser);
        }

        private void EditIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new EditIdeas(),UiHelper.GetUiHelper().GetIdeaListDictionary(_mLoggedUser));
        }

        private void FeedBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new mainWindow(),_mLoggedUser);
        }

        private void GroupsBtn_OnClick(object sender, RoutedEventArgs e)
        {
            //Switcher.Switch(new ,_mLoggedUser);
        }

        private void UserListBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new UserList(), UiHelper.GetUiHelper().GetUserListDictionary(_mLoggedUser));
        }
    }
}
