﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Implementations;
using Implementations.Database;
using Implementations.IdeasImplementation;
using Interfaces;

namespace Ideas.Views
{
    /// <summary>
    /// Interaction logic for EditIdea.xaml
    /// </summary>
    public partial class EditIdea : UserControl,ISwitchable
    {
        private IIdeas _idea;
        public EditIdea()
        {
            InitializeComponent();
            Loaded += delegate
            {
                Token.Focus();
            };

            Token.TokenMatcher = text =>
            {
                if (text.EndsWith(";"))
                {
                    // Remove the ';'
                    return text.Substring(0, text.Length - 1).Trim().ToUpper();
                }

                return null;
            };
        }

        private void TextBlockName_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            throw new NotImplementedException();
        }

        public void UtilizeState(object state)
        {
            _idea = state as IIdeas;
            if (_idea != null)
            {
                TextBlockName.Text = UiHelper.GetUiHelper().ReturnFullName(_idea.Owner);

                TitleInput.Text = _idea.Title;
                ContentInput.Text = _idea.Contennt;
                foreach (var tag in _idea.Tags)
                {
                    TextCompositionManager.StartComposition(
                        new TextComposition(InputManager.Current, Token, tag.TagName+";"));
                }
            }
        }

        private void UpdateIdea_OnClick(object sender, RoutedEventArgs e)
        {
            IIdeas aIdea = IdeaCreator.CreateIdea();
            aIdea.Owner = _idea.Owner;
            aIdea.Title = TitleInput.Text;
            aIdea.Contennt = ContentInput.Text;
            aIdea.Id = _idea.Id;
            aIdea.State = GeneralUtils.GetStatus(StateInput.Text);
            //TODO ASYNC
            SQLDB.GetDb().UpdateIdea(aIdea);
            SQLDB.GetDb().DeleteTags(aIdea);
            var content = Token.Document;
            SQLDB.GetDb().AddTags(aIdea,UiHelper.GetUiHelper().GetTagListFromFlowDoc(content));
            Switcher.Switch(new mainWindow(),_idea.Owner);
        }

        private void FeedBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new mainWindow(),_idea.Owner);
        }

        private void GroupBtn_OnClick(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void UserListBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new UserList(),_idea.Owner);
        }

        private void NewIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new NewIdea(),_idea.Owner);
        }

        private void EditIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new EditIdeas(),UiHelper.GetUiHelper().GetIdeaListDictionary(_idea.Owner));
        }
    }
}
