﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Implementations;
using Implementations.Database;
using Implementations.IdeasImplementation;
using Interfaces;
using Interfaces.Users;

namespace Ideas.Views
{
    /// <summary>
    /// Interaction logic for NewIdea.xaml
    /// </summary>
    public partial class NewIdea : UserControl,ISwitchable
    {
        private User _mLoggedUser;
        public NewIdea()
        {
            InitializeComponent();
            Loaded += delegate
            {
                Token.Focus();
            };

            Token.TokenMatcher = text =>
            {
                if (text.EndsWith(";"))
                {
                    // Remove the ';'
                    return text.Substring(0, text.Length - 1).Trim().ToUpper();
                }

                return null;
            };

        }

        public void UtilizeState(object state)
        {
            _mLoggedUser = state as User;
            if (_mLoggedUser != null)
            {
                TextBlockName.Text = _mLoggedUser.FirstName + " " + _mLoggedUser.LastName;
            
            }
        }

        private void SaveIdea_Click(object sender, RoutedEventArgs e)
        {
            IIdeas idea = IdeaCreator.CreateIdea();
            idea.Contennt = ContentInput.Text;
            idea.Title = TitleInput.Text;
            idea.State = GeneralUtils.GetStatus(StateInput.Text);
            idea.Owner = _mLoggedUser;
            var content = Token.Document;
            //TODO ASYNC
            SQLDB.GetDb().SaveIdea(idea);
            SQLDB.GetDb().AddTags(idea, UiHelper.GetUiHelper().GetTagListFromFlowDoc(content));
            Switcher.Switch(new mainWindow(), _mLoggedUser);
        }

        private void TextBlockName_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new PersonalPage(),_mLoggedUser);
        }

        private void NewIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new NewIdea(),_mLoggedUser);
        }

        private void EditIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new EditIdeas(),UiHelper.GetUiHelper().GetIdeaListDictionary(_mLoggedUser));
        }

        private void FeedBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new mainWindow(),_mLoggedUser);
        }

        private void GroupsBtn_OnClick(object sender, RoutedEventArgs e)
        {
            //Switcher.Switch(,_mLoggedUser);
        }

        private void UserListBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new UserList(),UiHelper.GetUiHelper().GetUserListDictionary(_mLoggedUser));
        }
    }
}
