﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Implementations.Database;
using Implementations.Subscription;
using Interfaces.Subsription;
using Interfaces.Users;

namespace Ideas.Views
{
    /// <summary>
    /// Interaction logic for UserList.xaml
    /// </summary>
    public partial class UserList : UserControl,ISwitchable
    {
        private User _mLoggedUser;
        public UserList()
        {
            InitializeComponent();
        }

        private void UserListDataGrid_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = ItemsControl.ContainerFromElement((DataGrid)sender, e.OriginalSource as DependencyObject) as DataGridRow;
            object data = row.Item;
            // extract the property value
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(data);
            PropertyDescriptor property = properties["Subscribed"];
            SubscriptionInvoker aInvoker = new SubscriptionInvoker();

            if (property.GetValue(data).ToString().Equals("X"))
            {
                property.SetValue(data, "");
                aInvoker.ChangeCommand(new UserUnsubscribe(_mLoggedUser.Email));
            }                
            else
            {
                property.SetValue(data, "X");
                aInvoker.ChangeCommand(new UserSubscription(_mLoggedUser.Email));
            }
            aInvoker.Execute(properties["Email"].GetValue(data).ToString());
        }

        public void UtilizeState(object state)
        {
            if (state != null)
            {
                var data  = (IDictionary<string, dynamic>) state;
                _mLoggedUser = data["User"];
                PopulateDataGrid((DataTable) data["DataTable"]);
                Name.Text = UiHelper.GetUiHelper().ReturnFullName(_mLoggedUser);
            }
        }     

        private void NewIdea_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new NewIdea(),_mLoggedUser);
        }

        private void EditIdea_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new EditIdeas(),UiHelper.GetUiHelper().GetIdeaListDictionary(_mLoggedUser));
        }

        private void Profile_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new PersonalPage(),_mLoggedUser);
        }


        private void FeedBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new mainWindow(),_mLoggedUser);
        }

        private void NewIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new NewIdea(),_mLoggedUser);
        }

        private void EditIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new EditIdeas(),UiHelper.GetUiHelper().GetIdeaListDictionary(_mLoggedUser));
        }

        private void PopulateDataGrid(DataTable aDataTable)
        {                                   
            UserListDataGrid.ItemsSource = UiHelper.GetUiHelper().
                SubscibedDataTable(aDataTable,SQLDB.GetDb().
                GetAllUserSubscription(_mLoggedUser.Email)).
                DefaultView;
        }
    }
}
