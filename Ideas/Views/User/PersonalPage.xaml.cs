﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Interfaces.Statistic;
using Interfaces.Users;

namespace Ideas.Views
{
    /// <summary>
    /// Interaction logic for PersonalPage.xaml
    /// </summary>
    public partial class PersonalPage : UserControl,ISwitchable
    {
        private User _mLoggedUser;
        public PersonalPage()
        {
            InitializeComponent();
        }

        public void UtilizeState(object state)
        {
            IDictionary<string,dynamic> passedObjects = state as Dictionary<string,dynamic>;
            _mLoggedUser = passedObjects["User"];
            IStatisticContainer stats = passedObjects["Stats"];

            TextBlockName.Text = UiHelper.GetUiHelper().ReturnFullName(_mLoggedUser);
            FnameText.Text = _mLoggedUser.FirstName;
            LnameText.Text = _mLoggedUser.LastName;
            EmailText.Text = _mLoggedUser.Email;
            CreationText.Text = stats.CreationDate.ToString();
            LastVisitText.Text = stats.LastVisit.ToString();
            TotalVisitsText.Text = stats.TotalVisits.ToString();
            TotalIDeasText.Text = stats.TotalIdeas.ToString();
            TotalCommentsText.Text = stats.TotalComments.ToString();
            stats.UserRating *= 100;
            UserRattingText.Text = stats.UserRating.ToString() + "%";
        }

        private void FeedBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new mainWindow(),_mLoggedUser);
        }

        private void GroupsBtn_OnClick(object sender, RoutedEventArgs e)
        {

        }

        private void UserListBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new UserList(),UiHelper.GetUiHelper().GetUserListDictionary(_mLoggedUser));
        }

        private void NewIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new NewIdea(),_mLoggedUser);
        }

        private void EditIdeaOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new EditIdeas(),UiHelper.GetUiHelper().GetIdeaListDictionary(_mLoggedUser));
        }

        private void LogOut_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new Login());
        }
    }
}
