﻿using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Implementations.Database;
using Interfaces.Users;
using MaterialDesignThemes.Wpf;

//TODO add logo image

namespace Ideas.Views
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : UserControl,ISwitchable
    {
        private readonly BackgroundWorker _worker = new BackgroundWorker();
        public Login()
        {
            InitializeComponent();
            _worker.DoWork += worker_DoWork;
            _worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            _worker.WorkerSupportsCancellation = true;
        }
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var email = e.Argument as string;
            if (email != null)
            {
                var user = SQLDB.GetDb().GetUserByEmail(email);                         
                if (user == null || _worker.CancellationPending || PasswordInput.Password != user.Password)
                {
                    e.Cancel = true;
                    return; 
                }
                Task.Delay(2000).Wait();
                e.Result = user;
            }
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled && e.Result != null)
            {
                var result = (User) e.Result;
                if (result != null)
                {
                    SQLDB.GetDb().IncrementVisits(result.Email);                                       
                    Switcher.Switch(new mainWindow(), result);
                    return;
                }
            }            
            Switcher.Switch(new Login());         
        }

        public void UtilizeState(object state)
        {
            var email = state as string;
            EmailInput.Text = email;
        }

        private void RegisterTextBlock_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new Register());
        }


        private void OnDialogClosingLoginDialog(object sender, DialogClosingEventArgs eventargs)
        {
            _worker.CancelAsync();
        }

        private void Login_OnClick(object sender, RoutedEventArgs e)
        {            
            _worker.RunWorkerAsync(EmailInput.Text);            
        }
    }
}
