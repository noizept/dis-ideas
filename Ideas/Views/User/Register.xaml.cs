﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Implementations.Database;
using MaterialDesignThemes.Wpf;

namespace Ideas.Views
{//TODO add logo
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class Register : UserControl, ISwitchable
    {
        private readonly Registar _worker = new Registar();

        public Register()
        {
            InitializeComponent();
            _worker.DoWork += worker_DoWork;
            _worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            _worker.WorkerSupportsCancellation = true;
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var regDetails = e.Argument as Dictionary<string,string>;
            var status = _worker.Register(regDetails["email"],
                regDetails["fName"],regDetails["lName"],
                regDetails["password"],regDetails["confPass"]);

            if (_worker.CancellationPending || !status)
            {
                e.Cancel = true;
                return;
            }
            Task.Delay(2500);
            Dictionary<string, dynamic> resultObjects = new Dictionary<string, dynamic>
            {
                {"email", regDetails["email"]},
                {"result", status}
            };
            e.Result = resultObjects;
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                Switcher.Switch(new Register());
                return;
            }
            var result = e.Result as Dictionary<string, dynamic>;
            if (result != null && result["result"])
            {
                Switcher.Switch(new Login(),result["email"]);
            }
        }

        public void UtilizeState(object state)
        {
            /*  if (state != null)
            {
                EmailInput.Text = state as string;
            }*/
        }

        private void Image_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Switcher.Switch(new Login());
        }

        private void OnDialogClosingRegisterDialog(object sender, DialogClosingEventArgs eventargs)
        {
            _worker.CancelAsync();
        }

        private void user_register(object sender, RoutedEventArgs e)
        {
            Dictionary<string,string> regValues = new Dictionary<string,string>();
            regValues.Add("email",EmailInput.Text);
            regValues.Add("fName",FirstNameInput.Text);
            regValues.Add("lName",LastNameInput.Text);
            regValues.Add("password",PasswordInput.Password);
            regValues.Add("confPass", PasswordValInput.Password);           
            _worker.RunWorkerAsync(regValues); 
        }
    }

    public static class Validator
    {
        public static bool IsValidEmailAddress(string emailAddress)
        {
            return new System.ComponentModel.DataAnnotations
                .EmailAddressAttribute()
                .IsValid(emailAddress);
        }

        public static bool IsEmpty(string toVerify)
        {
            return string.IsNullOrEmpty(toVerify);
        }
    }

    public class Registar : BackgroundWorker
    {
        public bool Register(string email, string fName, string lName, string pass,string confPass)
        {
            if (Validator.IsValidEmailAddress(email)
                && !Validator.IsEmpty(fName)
                && !Validator.IsEmpty(lName)
                && !Validator.IsEmpty(pass)
                && pass == confPass)
            {
                var db = SQLDB.GetDb();
                var newUser = new Hashtable
                {
                    {"email", email},
                    {"first_name", fName},
                    {"last_name", lName},
                    {"password", pass}
                };
                 return db.RegistUser(newUser);
            }
            return false;
        }
    }
}
