﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Documents;
using Implementations.Database;
using Interfaces;
using Interfaces.Users;

namespace Ideas
{
    public class UiHelper
    {
        private static UiHelper _helper;
        private UiHelper()
        {            
        }

        public static UiHelper GetUiHelper()
        {
            return _helper ?? (_helper = new UiHelper());
        }

        public IList<string> GetTagListFromFlowDoc(FlowDocument aDocument)
        {
            var range = new TextRange(aDocument.ContentStart, aDocument.ContentEnd);

            if (range.Start.Paragraph != null)
            {
                IList<string> tags = range.Start.Paragraph.Inlines.OfType<InlineUIContainer>().Select(c => c.Child).OfType<ContentPresenter>().Select(c => c.Content).OfType<string>().ToList();

                if (tags.Count > 0)
                {
                    tags = tags.Distinct().ToList();
                    return tags;
                }
                //var notTagsYet = range.Start.Paragraph.Inlines.OfType<Run>().Select(c => c.Text).ToList();                

            }
            return null;
        }

        public string ReturnFullName(User aUser)
        {
            return aUser.FirstName + " " + aUser.LastName;
        }

        public IDictionary<string, dynamic> GetUserListDictionary(User mLoggedUser)
        {
            var aDataTableUserList = SQLDB.GetDb().GetAllUsersDataTable();
            var passToView = new Dictionary<string, dynamic>();
            passToView.Add("User", mLoggedUser);
            passToView.Add("DataTable", aDataTableUserList);
            return passToView;
        }

        public IDictionary<string, dynamic> GetIdeaListDictionary(User mLoggedUser)
        {
            var ideas = SQLDB.GetDb().GetIdeasDataTable(mLoggedUser);
            IDictionary<string, dynamic> passToView = new Dictionary<string, dynamic>();
            passToView.Add("User", mLoggedUser);
            passToView.Add("Ideas", ideas);
            return passToView;
        }

        public DataTable SubscibedDataTable (DataTable aDataTable, IList<string> aList )
        {
            for (int i = 0; i < aDataTable.Rows.Count-1; i++)
            {
                if (aList.Contains(aDataTable.Rows[i][0]))
                {
                    aDataTable.Rows[i][3] = "X";
                }
            }
            return aDataTable;
        }
    }
}