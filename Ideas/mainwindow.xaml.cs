﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ideas.Views;
using Implementations.Database;
using Interfaces.Users;

namespace Ideas
{
    /// <summary>
    /// Interaction logic for mainwindow.xaml
    /// </summary>
    public partial class mainwindow : Window
    {
        private static User _mLoggedUser;
        public mainwindow()
        {
            InitializeComponent();
            Switcher.Mainwindow = this;
            Switcher.Switch(new Login());
        }
        public void Navigate(UserControl nextPage)
        {
            this.Content = nextPage;
        }

        /// <exception cref="ArgumentException">Condition.</exception>
        public void Navigate(UserControl nextPage, object state)
        {
            this.Content = nextPage;
            ISwitchable s = nextPage as ISwitchable;

            if (s != null)
                s.UtilizeState(state);
            else
                throw new ArgumentException("NextPage is not ISwitchable! "
                  + nextPage.Name.ToString());
        }

    }
}
